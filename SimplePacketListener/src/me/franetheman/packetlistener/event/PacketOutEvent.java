package me.franetheman.packetlistener.event;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import me.franetheman.packetlistener.listener.PacketOutType;

public class PacketOutEvent extends Event implements Cancellable {

	private static final HandlerList HANDLERS = new HandlerList();
	private boolean cancelled;

	private Player player;
	private PacketOutType type;
	private Object packet;

	public PacketOutEvent(Player player, PacketOutType type, Object packet) {
		this.player = player;
		this.type = type;
		this.packet = packet;
	}

	public Player getPlayer() {
		return player;
	}

	public PacketOutType getType() {
		return type;
	}

	public Object getPacket() {
		return packet;
	}

	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public void setCancelled(boolean cancel) {
		this.cancelled = cancel;
	}

	@Override
	public HandlerList getHandlers() {
		return HANDLERS;
	}

	public static HandlerList getHandlerList() {
		return HANDLERS;
	}
}
