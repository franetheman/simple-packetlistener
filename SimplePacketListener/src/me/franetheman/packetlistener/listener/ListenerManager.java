package me.franetheman.packetlistener.listener;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class ListenerManager implements Listener {

	private Set<PacketInType> registeredIncomingPackets = new HashSet<>();
	private Set<PacketOutType> registeredOutgoingPackets = new HashSet<>();

	public ListenerManager(JavaPlugin plugin) {
		Bukkit.getPluginManager().registerEvents(this, plugin);
	}

	// incoming
	public void listenForIncomingPacket(PacketInType type) {
		registeredIncomingPackets.add(type);
	}

	public void removeIncomingPacketListener(PacketInType type) {
		if (!registeredIncomingPackets.contains(type))
			return;

		else
			registeredIncomingPackets.remove(type);
	}

	public boolean isListeningForIncomingPacket(PacketInType type) {
		return registeredIncomingPackets.contains(type);
	}

	// outgoing
	public void listenForOutgoingPacket(PacketOutType type) {
		registeredOutgoingPackets.add(type);
	}

	public void removeOutgoingPacketListener(PacketOutType type) {
		if (!registeredOutgoingPackets.contains(type))
			return;

		else
			registeredOutgoingPackets.remove(type);
	}

	public boolean isListeningForOutgoingPacket(PacketOutType type) {
		return registeredOutgoingPackets.contains(type);
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		((CraftPlayer) e.getPlayer()).getHandle().playerConnection = new ListeningConnection(this,
				((CraftPlayer) e.getPlayer()).getHandle());
	}
}
