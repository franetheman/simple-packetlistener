package me.franetheman.packetlistener.listener;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import me.franetheman.packetlistener.event.PacketInEvent;
import me.franetheman.packetlistener.event.PacketOutEvent;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import net.minecraft.server.v1_8_R3.MinecraftServer;
import net.minecraft.server.v1_8_R3.Packet;
import net.minecraft.server.v1_8_R3.PacketPlayInAbilities;
import net.minecraft.server.v1_8_R3.PacketPlayInArmAnimation;
import net.minecraft.server.v1_8_R3.PacketPlayInBlockDig;
import net.minecraft.server.v1_8_R3.PacketPlayInBlockPlace;
import net.minecraft.server.v1_8_R3.PacketPlayInChat;
import net.minecraft.server.v1_8_R3.PacketPlayInClientCommand;
import net.minecraft.server.v1_8_R3.PacketPlayInCloseWindow;
import net.minecraft.server.v1_8_R3.PacketPlayInCustomPayload;
import net.minecraft.server.v1_8_R3.PacketPlayInEnchantItem;
import net.minecraft.server.v1_8_R3.PacketPlayInEntityAction;
import net.minecraft.server.v1_8_R3.PacketPlayInFlying;
import net.minecraft.server.v1_8_R3.PacketPlayInHeldItemSlot;
import net.minecraft.server.v1_8_R3.PacketPlayInKeepAlive;
import net.minecraft.server.v1_8_R3.PacketPlayInResourcePackStatus;
import net.minecraft.server.v1_8_R3.PacketPlayInSetCreativeSlot;
import net.minecraft.server.v1_8_R3.PacketPlayInSettings;
import net.minecraft.server.v1_8_R3.PacketPlayInSpectate;
import net.minecraft.server.v1_8_R3.PacketPlayInSteerVehicle;
import net.minecraft.server.v1_8_R3.PacketPlayInTabComplete;
import net.minecraft.server.v1_8_R3.PacketPlayInTransaction;
import net.minecraft.server.v1_8_R3.PacketPlayInUpdateSign;
import net.minecraft.server.v1_8_R3.PacketPlayInUseEntity;
import net.minecraft.server.v1_8_R3.PacketPlayInWindowClick;
import net.minecraft.server.v1_8_R3.PlayerConnection;

public class ListeningConnection extends PlayerConnection{

	private ListenerManager manager;
	private Player connectionOwner;
	
	public ListeningConnection(ListenerManager manager, EntityPlayer entityplayer) {
		super(MinecraftServer.getServer(), entityplayer.playerConnection.networkManager, entityplayer);
		this.manager = manager;
		this.connectionOwner = entityplayer.getBukkitEntity();
	}
	
	@Override
	public void a(PacketPlayInAbilities packetplayinabilities) {
		if(manager.isListeningForIncomingPacket(PacketInType.ABILITIES)){
			PacketInEvent event = new PacketInEvent(connectionOwner, PacketInType.ABILITIES, packetplayinabilities);
			Bukkit.getPluginManager().callEvent(event);
			if(event.isCancelled())
				return;
			
			super.a(packetplayinabilities);
		}
		super.a(packetplayinabilities);
	}

	@Override
	public void a(PacketPlayInArmAnimation packetplayinarmanimation) {
		if(manager.isListeningForIncomingPacket(PacketInType.ARM_ANIMATION)){
			System.out.println("debug");
			PacketInEvent event = new PacketInEvent(connectionOwner, PacketInType.ARM_ANIMATION, packetplayinarmanimation);
			Bukkit.getPluginManager().callEvent(event);
			if(event.isCancelled())
				return;
			
			super.a(packetplayinarmanimation);
		}
		super.a(packetplayinarmanimation);
	}

	@Override
	public void a(PacketPlayInBlockDig packetplayinblockdig) {
		if(manager.isListeningForIncomingPacket(PacketInType.BLOCK_DIG)){
			PacketInEvent event = new PacketInEvent(connectionOwner, PacketInType.BLOCK_DIG, packetplayinblockdig);
			Bukkit.getPluginManager().callEvent(event);
			if(event.isCancelled())
				return;
			
			super.a(packetplayinblockdig);
		}
		super.a(packetplayinblockdig);
	}

	@Override
	public void a(PacketPlayInBlockPlace packetplayinblockplace) {
		if(manager.isListeningForIncomingPacket(PacketInType.BLOCK_PLACE)){
			PacketInEvent event = new PacketInEvent(connectionOwner, PacketInType.BLOCK_PLACE, packetplayinblockplace);
			Bukkit.getPluginManager().callEvent(event);
			if(event.isCancelled())
				return;
			
			super.a(packetplayinblockplace);
		}
		super.a(packetplayinblockplace);
	}

	@Override
	public void a(PacketPlayInChat packetplayinchat) {
		if(manager.isListeningForIncomingPacket(PacketInType.CHAT)){
			PacketInEvent event = new PacketInEvent(connectionOwner, PacketInType.CHAT, packetplayinchat);
			Bukkit.getPluginManager().callEvent(event);
			if(event.isCancelled())
				return;
			
			super.a(packetplayinchat);
		}
		super.a(packetplayinchat);
	}

	@Override
	public void a(PacketPlayInClientCommand packetplayinclientcommand) {
		if(manager.isListeningForIncomingPacket(PacketInType.CLIENT_COMMAND)){
			PacketInEvent event = new PacketInEvent(connectionOwner, PacketInType.CLIENT_COMMAND, packetplayinclientcommand);
			Bukkit.getPluginManager().callEvent(event);
			if(event.isCancelled())
				return;
			
			super.a(packetplayinclientcommand);
		}
		super.a(packetplayinclientcommand);
	}

	@Override
	public void a(PacketPlayInCloseWindow packetplayinclosewindow) {
		if(manager.isListeningForIncomingPacket(PacketInType.CLOSE_WINDOW)){
			PacketInEvent event = new PacketInEvent(connectionOwner, PacketInType.CLOSE_WINDOW, packetplayinclosewindow);
			Bukkit.getPluginManager().callEvent(event);
			if(event.isCancelled())
				return;
			
			super.a(packetplayinclosewindow);
		}
		super.a(packetplayinclosewindow);
	}

	@Override
	public void a(PacketPlayInCustomPayload packetplayincustompayload) {
		if(manager.isListeningForIncomingPacket(PacketInType.CUSTOM_PAYLOAD)){
			PacketInEvent event = new PacketInEvent(connectionOwner, PacketInType.CUSTOM_PAYLOAD, packetplayincustompayload);
			Bukkit.getPluginManager().callEvent(event);
			if(event.isCancelled())
				return;
			
			super.a(packetplayincustompayload);
		}
		super.a(packetplayincustompayload);
	}

	@Override
	public void a(PacketPlayInEnchantItem packetplayinenchantitem) {
		if(manager.isListeningForIncomingPacket(PacketInType.ENCHANT_ITEM)){
			PacketInEvent event = new PacketInEvent(connectionOwner, PacketInType.ENCHANT_ITEM, packetplayinenchantitem);
			Bukkit.getPluginManager().callEvent(event);
			if(event.isCancelled())
				return;
			
			super.a(packetplayinenchantitem);
		}
		super.a(packetplayinenchantitem);
	}

	@Override
	public void a(PacketPlayInEntityAction packetplayinentityaction) {
		if(manager.isListeningForIncomingPacket(PacketInType.ENTITY_ACTION)){
			PacketInEvent event = new PacketInEvent(connectionOwner, PacketInType.ENTITY_ACTION, packetplayinentityaction);
			Bukkit.getPluginManager().callEvent(event);
			if(event.isCancelled())
				return;
			
			super.a(packetplayinentityaction);
		}
		super.a(packetplayinentityaction);
	}

	@Override
	public void a(PacketPlayInFlying packetplayinflying) {
		if(manager.isListeningForIncomingPacket(PacketInType.FLYING)){
			PacketInEvent event = new PacketInEvent(connectionOwner, PacketInType.FLYING, packetplayinflying);
			Bukkit.getPluginManager().callEvent(event);
			if(event.isCancelled())
				return;
			
			super.a(packetplayinflying);
		}
		super.a(packetplayinflying);
	}

	@Override
	public void a(PacketPlayInHeldItemSlot packetplayinhelditemslot) {
		if(manager.isListeningForIncomingPacket(PacketInType.HELD_ITEM_SLOT)){
			PacketInEvent event = new PacketInEvent(connectionOwner, PacketInType.HELD_ITEM_SLOT, packetplayinhelditemslot);
			Bukkit.getPluginManager().callEvent(event);
			if(event.isCancelled())
				return;
			
			super.a(packetplayinhelditemslot);
		}
		super.a(packetplayinhelditemslot);
	}

	@Override
	public void a(PacketPlayInKeepAlive packetplayinkeepalive) {
		if(manager.isListeningForIncomingPacket(PacketInType.KEEP_ALIVE)){
			PacketInEvent event = new PacketInEvent(connectionOwner, PacketInType.KEEP_ALIVE, packetplayinkeepalive);
			Bukkit.getPluginManager().callEvent(event);
			if(event.isCancelled())
				return;
			
			super.a(packetplayinkeepalive);
		}
		super.a(packetplayinkeepalive);
	}

	@Override
	public void a(PacketPlayInResourcePackStatus packetplayinresourcepackstatus) {
		if(manager.isListeningForIncomingPacket(PacketInType.RESOURCEPACK_STATUS)){
			PacketInEvent event = new PacketInEvent(connectionOwner, PacketInType.RESOURCEPACK_STATUS, packetplayinresourcepackstatus);
			Bukkit.getPluginManager().callEvent(event);
			if(event.isCancelled())
				return;
			
			super.a(packetplayinresourcepackstatus);
		}
		super.a(packetplayinresourcepackstatus);
	}

	@Override
	public void a(PacketPlayInSetCreativeSlot packetplayinsetcreativeslot) {
		if(manager.isListeningForIncomingPacket(PacketInType.SET_CREATIVE_SLOT)){
			PacketInEvent event = new PacketInEvent(connectionOwner, PacketInType.SET_CREATIVE_SLOT, packetplayinsetcreativeslot);
			Bukkit.getPluginManager().callEvent(event);
			if(event.isCancelled())
				return;
			
			super.a(packetplayinsetcreativeslot);
		}
		super.a(packetplayinsetcreativeslot);
	}

	@Override
	public void a(PacketPlayInSettings packetplayinsettings) {
		if(manager.isListeningForIncomingPacket(PacketInType.SETTINGS)){
			PacketInEvent event = new PacketInEvent(connectionOwner, PacketInType.SETTINGS, packetplayinsettings);
			Bukkit.getPluginManager().callEvent(event);
			if(event.isCancelled())
				return;
			
			super.a(packetplayinsettings);
		}
		super.a(packetplayinsettings);
	}

	@Override
	public void a(PacketPlayInSpectate packetplayinspectate) {
		if(manager.isListeningForIncomingPacket(PacketInType.SPECTATE)){
			PacketInEvent event = new PacketInEvent(connectionOwner, PacketInType.SPECTATE, packetplayinspectate);
			Bukkit.getPluginManager().callEvent(event);
			if(event.isCancelled())
				return;
			
			super.a(packetplayinspectate);
		}
		super.a(packetplayinspectate);
	}

	@Override
	public void a(PacketPlayInSteerVehicle packetplayinsteervehicle) {
		if(manager.isListeningForIncomingPacket(PacketInType.STEER_VEHICLE)){
			PacketInEvent event = new PacketInEvent(connectionOwner, PacketInType.STEER_VEHICLE, packetplayinsteervehicle);
			Bukkit.getPluginManager().callEvent(event);
			if(event.isCancelled())
				return;
			
			super.a(packetplayinsteervehicle);
		}
		super.a(packetplayinsteervehicle);
	}

	@Override
	public void a(PacketPlayInTabComplete packetplayintabcomplete) {
		if(manager.isListeningForIncomingPacket(PacketInType.TAB_COMPLETE)){
			PacketInEvent event = new PacketInEvent(connectionOwner, PacketInType.TAB_COMPLETE, packetplayintabcomplete);
			Bukkit.getPluginManager().callEvent(event);
			if(event.isCancelled())
				return;
			
			super.a(packetplayintabcomplete);
		}
		super.a(packetplayintabcomplete);
	}

	@Override
	public void a(PacketPlayInTransaction packetplayintransaction) {
		if(manager.isListeningForIncomingPacket(PacketInType.TRANSACTION)){
			PacketInEvent event = new PacketInEvent(connectionOwner, PacketInType.TRANSACTION, packetplayintransaction);
			Bukkit.getPluginManager().callEvent(event);
			if(event.isCancelled())
				return;
			
			super.a(packetplayintransaction);
		}
		super.a(packetplayintransaction);
	}

	@Override
	public void a(PacketPlayInUpdateSign packetplayinupdatesign) {
		if(manager.isListeningForIncomingPacket(PacketInType.UPDATE_SIGN)){
			PacketInEvent event = new PacketInEvent(connectionOwner, PacketInType.UPDATE_SIGN, packetplayinupdatesign);
			Bukkit.getPluginManager().callEvent(event);
			if(event.isCancelled())
				return;
			
			super.a(packetplayinupdatesign);
		}
		super.a(packetplayinupdatesign);
	}

	@Override
	public void a(PacketPlayInUseEntity packetplayinuseentity) {
		if(manager.isListeningForIncomingPacket(PacketInType.USE_ENTITY)){
			PacketInEvent event = new PacketInEvent(connectionOwner, PacketInType.USE_ENTITY, packetplayinuseentity);
			Bukkit.getPluginManager().callEvent(event);
			if(event.isCancelled())
				return;
			
			super.a(packetplayinuseentity);
		}
		super.a(packetplayinuseentity);
	}

	@Override
	public void a(PacketPlayInWindowClick packetplayinwindowclick) {
		if(manager.isListeningForIncomingPacket(PacketInType.WINDOW_CLICK)){
			PacketInEvent event = new PacketInEvent(connectionOwner, PacketInType.WINDOW_CLICK, packetplayinwindowclick);
			Bukkit.getPluginManager().callEvent(event);
			if(event.isCancelled())
				return;
			
			super.a(packetplayinwindowclick);
		}
		super.a(packetplayinwindowclick);
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public void sendPacket(Packet packet) {
		String packetName = packet.getClass().getSimpleName();
		PacketOutType type = PacketOutType.getTypeForName(packetName);
		if(manager.isListeningForOutgoingPacket(type)){
			PacketOutEvent event = new PacketOutEvent(connectionOwner, type, packet);
			Bukkit.getPluginManager().callEvent(event);
			if(event.isCancelled())
				return;
			
			else
				super.sendPacket(packet);
		}
		super.sendPacket(packet);
	}
}
