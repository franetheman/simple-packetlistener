package me.franetheman.packetlistener.listener;

public enum PacketOutType {

	ABILITIES("PacketPlayOutAbilities"),
	ANIMATION("PacketPlayOutAnimation"),
	ATTACH_ENTITY("PacketPlayOutAttachEntity"),
	BED("PacketPlayOutBed"),
	BLOCK_ACTION("PacketPlayOutBlockAction"),
	BLOCK_BREAK_ANIMATION("PacketPlayOutBlockBreakAnimation"),
	BLOCK_CHANGE("PacketPlayOutBlockChange"),
	CAMERA("PacketPlayOutCamera"),
	CHAT("PacketPlayOutChat"),
	CLOSE_WINDOW("PacketPlayOutCloseWindow"),
	COLLECT("PacketPlayOutCollect"),
	COMBAT_EVENT("PacketPlayOutCombatEvent"),
	CUSTOM_PAYLOAD("PacketPlayOutCustomPayload"),
	ENTITY("PacketPlayOutEntity"),
	REL_ENTITY_MOVE_LOOK("PacketPlayOutRelEntityMoveLook"),
	REL_ENTITY_MOVE("PacketPlayOutRelEntityMove"),
	REL_ENTITY_LOOK("PacketPlayOutEntityLook"),
	ENTITY_DESTROY("PacketPlayOutEntityDestroy"),
	ENTITY_EFFECT("PacketPlayOutEntityEffect"),
	ENTITY_EQUIPMENT("PacketPlayOutEntityEquipment"),
	ENTITY_HEAD_ROTATION("PacketPlayOutEntityHeadRotation"),
	ENTITY_METADATA("PacketPlayOutEntityMetadata"),
	ENTITY_STATUS("PacketPlayOutEntityStatus"),
	ENTITY_TELEPORT("PacketPlayOutEntityTeleport"),
	ENTITY_VELOCITY("PacketPlayOutEntityVelocity"),
	EXPERIENCE("PacketPlayOutExperience"),
	EXPLOSION("PacketPlayOutExplosion"),
	GAME_STATE_CHANGE("PacketPlayOutGameStateChange"),
	HELD_ITEM_SLOT("PacketPlayOutHeldItemSlot"),
	KEEP_ALIVE("PacketPlayOutKeepAlive"),
	KICK_DISCONNECT("PacketPlayOutKickDisconnect"),
	LOGIN("PacketPlayOutLogin"),
	MAP("PacketPlayOutMap"),
	MAP_CHUNK("PacketPlayOutMapChunk"),
	MAP_CHUNK_BULK("PacketPlayOutMapChunkBulk"),
	MULTI_BLOCK_CHANGE("PacketPlayOutMultiBlockChange"),
	NAMED_ENTITY_SPAWN("PacketPlayOutNamedEntitySpawn"),
	NAMED_SOUND_EFFECT("PacketPlayOutNamedSoundEffect"),
	OPEN_SIGN_EDITOR("PacketPlayOutOpenSignEditor"),
	OPEN_WINDOW("PacketPlayOutOpenWindow"),
	PLAYER_INFO("PacketPlayOutPlayerInfo"),
	PLAYER_LIST_HEADER_FOOTER("PacketPlayOutPlayerListHeaderFooter"),
	PLAYER_POSITION("PacketPlayOutPosition"),
	REMOVE_ENTITY_EFFECT("PacketPlayOutRemoveEntityEffect"),
	RESOURCEPACK_SEND("PacketPlayOutResourcePackSend"),
	RESPAWN("PacketPlayOutRespawn"),
	SCOREBOARD_DISPLAY_OBJECTIVE("PacketPlayOutScoreboardDisplayObjective"),
	SCOREBOARD_OBJECTIVE("PacketPlayOutScoreboardObjective"),
	SCOREBOARD_SCORE("PacketPlayOutScoreboardScore"),
	SCOREBOARD_TEAM("PacketPlayOutScoreboardTeam"),
	SERVER_DIFFICULTY("PacketPlayOutServerDifficulty"),
	SET_COMPRESSION("PacketPlayOutSetCompression"),
	SET_SLOT("PacketPlayOutSetSlot"),
	SPAWN_ENTITY("PacketPlayOutSpawnEntity"),
	SPAWN_ENTITY_EXPERIENCE_ORB("PacketPlayOutSpawnEntityExperienceOrb"),
	SPAWN_ENTITY_LIVING("PacketPlayOutSpawnEntityLiving"),
	SPAWN_ENTITY_PAINTING("PacketPlayOutSpawnEntityPainting"),
	SPAWN_ENTITY_WEATHER("PacketPlayOutSpawnEntityWeather"),
	SPAWN_POSITION("PacketPlayOutSpawnPosition"),
	STATISTIC("PacketPlayOutStatistic"),
	TAB_COMPLETE("PacketPlayOutTabComplete"),
	TILE_ENTITY_DATA("PacketPlayOutTileEntityData"),
	TITLE("PacketPlayOutTitle"),
	TRANSACTION("PacketPlayOutTransaction"),
	UPDATE_ATTRIBUTES("PacketPlayOutUpdateAttributes"),
	UPDATE_ENTITY_NBT("PacketPlayOutUpdateEntityNBT"),
	UPDATE_HEALTH("PacketPlayOutUpdateHealth"),
	UPDATE_SIGN("PacketPlayOutUpdateSign"),
	UPDATE_TIME("PacketPlayOutUpdateTime"),
	UPDATE_WINDOW_DATA("PacketPlayOutWindowData"),
	UPDATE_WINDOW_ITEMS("PacketPlayOutWindowItems"),
	WORLD_BORDER("PacketPlayOutWorldBorder"),
	WORLD_EVENT("PacketPlayOutWorldEvent"),
	WORLD_PARTICLES("PacketPlayOutWorldParticles");
	
	private String packetName;
	
	private PacketOutType(String packetName){
		this.packetName = packetName;
	}
	
	public String getPacketName(){
		return this.packetName;
	}
	
	public static PacketOutType getTypeForName(String name){
		for(PacketOutType type : PacketOutType.values()){
			if(type.getPacketName().equals(name))
				return type;
		}
		return null;
	}
}
